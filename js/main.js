const tempTracker = new TempTracker();

function onClickInsert() {
    //triggers to insert the value
    tempTracker.insert = document.getElementById('temperature-input').value;

    //generate string to show all temperatures
    document.getElementById('label-for-all-temps').innerText = tempTracker.getAll.reduce((a, b) => {
        return `${a}, `+`${b}`;
    });

    //insert respective temperatures for highest, lowest and average
    document.getElementById('label-for-highest-temps').innerText = tempTracker.getMax;
    document.getElementById('label-for-lowest-temps').innerText = tempTracker.getMin;
    document.getElementById('label-for-avg-temps').innerText = tempTracker.getAvg;

    //to auto clear the text field after each entry
    document.getElementById('temperature-input').value = '';
}
