class TempTracker {
    constructor() {
        this.max = 0;
        this.min = 0;
        this.avg = 0;
        this.sum = 0;
        this.count = 0;
        this.allTemp = [];
    }

    set insert(temp) {
        temp = parseFloat(temp);

        //check if min doesnt exist or that min is smaller than
        //the entered value then set new value to min
        if(!this.min || this.min > temp) {
            this.min = temp;
        }

        //check if max doesnt exist or that max is larger than
        //the entered value then set new value to max
        if(!this.max || this.max < temp) {
            this.max = temp;
        }

        //pushing all entries into an array to retrieve all temperatures later
        this.allTemp.push(temp);

        //sum and count used for calculating average temperature
        this.sum += temp;
        this.count++;
        this.avg = (this.sum/this.count).toFixed(2);
    }

    get getMax() {
        return this.max;
    }

    get getMin() {
        return this.min;
    }

    get getAvg() {
        return this.avg;
    }

    get getAll() {
        return this.allTemp;
    }
}
